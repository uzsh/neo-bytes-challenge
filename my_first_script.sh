stages:
  - build
  - test
  - deploy

build:
  stage: build
  script:
    - echo "Building the 'Hello World' app"

test:
  stage: test
  script:
    - echo "Testing the 'Hello World' app"

deploy:
  stage: deploy
  script:
    - echo "Deploying the 'Hello World' app"
